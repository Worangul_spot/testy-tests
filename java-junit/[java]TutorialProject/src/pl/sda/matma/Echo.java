package pl.sda.matma;

import java.util.Scanner;

/**
 * Created by Acer on 2019-01-12.
 */
public class Echo {
    public static void main(String[] args) {
        Scanner textScan = new Scanner(System.in);

        String tekst = textScan.nextLine();
        System.out.println(tekst.toUpperCase());
    }
}
