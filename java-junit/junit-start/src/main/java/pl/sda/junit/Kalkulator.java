package pl.sda.junit;

/**
 * Created by Acer on 2019-01-13.
 */
public class Kalkulator {
    public static int potega(int podstawa, int wykladnik)
    {
        int wynik = 1;
        for (int i=1; i<=wykladnik; i++)
        {
            wynik=podstawa*wynik;
        }
        return wynik;
    }
    public static boolean czyLiczbaPodzielnaPrzez(int liczba, int dzielnik)
    {
        if(dzielnik!=0)
        {
            return liczba % dzielnik == 0;
        }
        else
        {
            return false;
        }
    }
}

