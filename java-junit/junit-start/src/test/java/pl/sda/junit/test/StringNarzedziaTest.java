package pl.sda.junit.test;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.junit.StringNarzedzia;

/**
 * Created by Acer on 2019-01-13.
 */

public class StringNarzedziaTest {
    @Test
    public void wszystkieDuzeZnaki()
    {
        Assert.assertEquals("TEST",StringNarzedzia.zamieniamNaDuzeZnaki("test"));
    }
    @Test
    public void alaMaKotaTest()
    {
        Assert.assertEquals("ALA MA KOTA", StringNarzedzia.zamieniamNaDuzeZnaki("aLa Ma koTa") );
    }
}
