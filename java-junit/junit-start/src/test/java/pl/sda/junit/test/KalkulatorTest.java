package pl.sda.junit.test;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.junit.Kalkulator;

/**
 * Created by Acer on 2019-01-13.
 */
public class KalkulatorTest
{
    @Test
    public void zeroDoOsmejDajeZero()
    {
        int wynik = Kalkulator.potega(0,8);
        Assert.assertEquals(0,wynik);
    }
    @Test
    public void trzyDoZerowejDajeJeden()
    {
        Assert.assertEquals(1,Kalkulator.potega(3,0));
    }
    @Test
    public void dwaDoPiątejDajeTrzydziesciDwa()
    {
        Assert.assertEquals(32,Kalkulator.potega(2,5));
    }
    @Test
    public void siedemNieJestPodzielnePrzezPiec()
    {
        Assert.assertEquals(false, Kalkulator.czyLiczbaPodzielnaPrzez(7,5));
    }
    @Test
    public void osiemJestPodzielnePrzezDwa()
    {
        Assert.assertEquals(true, Kalkulator.czyLiczbaPodzielnaPrzez(8,2));
    }
    @Test
    public void siedemNieJestPodzielnePrzezZero()
    {
        Assert.assertEquals(false, Kalkulator.czyLiczbaPodzielnaPrzez(7,0));
    }
    @Test
    public void dziewiecDziesiatDziewiecNieJestPodzielnePrzezZero()
    {
        Assert.assertEquals(false, Kalkulator.czyLiczbaPodzielnaPrzez(99,0));
    }
}
