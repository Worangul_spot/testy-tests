package pl.sda.petle.petlaFor;

import java.util.Scanner;

/**
 * Created by Acer on 2019-01-12.
 */
public class DrukujLiczbyParzyste {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int liczba = scanner.nextInt();

        for (int i = 0; i<=liczba; i++)
        {
            if(i%2==0) {
                System.out.println(i);
            }
        }
    }
}
