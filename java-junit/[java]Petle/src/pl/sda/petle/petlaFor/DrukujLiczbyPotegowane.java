package pl.sda.petle.petlaFor;

import java.util.Scanner;
import java.math.*;

/**
 * Created by Acer on 2019-01-12.
 */
public class DrukujLiczbyPotegowane {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę podstawową");
        int liczba = scanner.nextInt();
        System.out.println("Podaj wykładnik potęgi");
        int wykladnik = scanner.nextInt();
        int wynik;
        wynik=1;

        if(wykladnik>0) {
            for (int i = 1; i <= wykladnik; i++) {
                wynik *= liczba;
            }
        }
        System.out.println("Wynik potęgi liczby "+liczba+" o wykładniku "+wykladnik+" wynosi:"+wynik);
    }
}
