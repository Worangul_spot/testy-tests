package sample;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class SwingControlDemo{
private JFrame mainframe;
private JPanel controlPanel;
private JLabel headerLabel;
private JLabel statusLabel;

private SwingControlDemo()
{
    prepareGUI();
}
public static void main(String[] args){
    SwingControlDemo swingControlDemo= new SwingControlDemo();
    swingControlDemo.showEventDemo();
}

private void prepareGUI()
{
    mainframe = new JFrame("Java SWING examples");
    mainframe.setSize(400,400);
    mainframe.setLayout(new GridLayout(3,1));

    headerLabel = new JLabel("", JLabel.CENTER);
    statusLabel = new JLabel("",JLabel.CENTER);

    mainframe.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent windowEvent){
            System.exit(0);
        }
    });
    controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout());

    mainframe.add(headerLabel);
    mainframe.add(controlPanel);
    mainframe.add(statusLabel);
    mainframe.setVisible(true);
}
private void showEventDemo(){
    headerLabel.setText("Control in action: button");
    JButton okButton = new JButton("OK");
    JButton submitButton = new JButton("Submit");
    JButton cancelButton = new JButton("Cancel");

    okButton.setActionCommand("Ok");
    submitButton.setActionCommand("Submit");
    cancelButton.setActionCommand("Cancel");

    okButton.addActionListener(new ButtonClickListener());
    submitButton.addActionListener(new ButtonClickListener());
    cancelButton.addActionListener(new ButtonClickListener());

    controlPanel.add(okButton);
    controlPanel.add(submitButton);
    controlPanel.add(cancelButton);

    mainframe.setVisible(true);
}
private class ButtonClickListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String command = e.getActionCommand();
            if(command == "Ok")
            {
                statusLabel.setText("OK button is clicked.");
            }
            else if(command == "Submit")
            {
                statusLabel.setText("Submit button is clicked.");
            }
            else if(command == "Cancel")
            {
                statusLabel.setText("Cancel button is clicked.");
            }
        }
    }
}
