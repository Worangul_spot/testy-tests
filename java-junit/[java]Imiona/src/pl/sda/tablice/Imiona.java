package pl.sda.tablice;

/**
 * Created by Acer on 2019-01-13.
 */
public class Imiona {
    public static void main(String[] args) {
        String[] imiona = new String[]{"Halyna", "Grażyna", "Seba", "Pjoter", "Somsiad", "Janusz"};

        wyswietlWszystkieMetody(imiona);
        czyKobieceImie(imiona);
        czyMeskieImie(imiona);
    }

    private static void czyMeskieImie(String[] imiona) {
        System.out.println("Tylko panowie");
        for(String imie:imiona) {
            if (!czyKobieceImie(imie)) {
                System.out.println(imie);
            }
        }
    }

    private static boolean czyKobieceImie(String imie) {
        if (imie.endsWith("a") && !imie.equals("Seba")) {
            System.out.println(imie);
            return true;
        }
        return false;
    }

    private static void czyKobieceImie(String[] imiona) {

        for(int i = 0; i< imiona.length; i++) {
            if (imiona[i].endsWith("a") && !imiona.equals("Seba")) {
                System.out.println(imiona[i]);
            }
        }
    }

    public static void wyswietlWszystkieMetody(String[] imiona) {
        for(int i = 0; i < imiona.length; i++)
        {
            System.out.println(imiona[i]);
        }
    }
}
