package pl.sda.tablice;

/**
 * Created by Acer on 2019-01-13.
 */
public class Liczby {
    public static void main(String[] args) {
        int[] liczby = new int[]{1, 3, 5, 10};
        int i;
        System.out.println("Zadanie2: liczba o indekse 3: " + liczby[3]);
        System.out.println("Zadanie3: wypisz elementy w pętli:");
        for (i = 0; i < liczby.length; i++) {
            System.out.println("Liczba o indekse " + i + " to " + liczby[i]);
        }
        System.out.println("Zdanie4: wypisz elementy o parzystym indekse:");
        for (i = 0; i < liczby.length; i++) {
            if (i % 2 == 0) {
                System.out.println(liczby[i]);
            }
        }
        System.out.println("Zadanie5: wypisz tylko liczby parzyste:");
        for (i = 0; i < liczby.length; i++) {
            if (liczby[i] % 2 == 0) {
                System.out.println(liczby[i]);
            }
        }
        System.out.println("Zadanie6: wypisz elementy w odwróconej kolejności:");
        for (i = liczby.length - 1; i >= 0; i--) {
            System.out.println(liczby[i]);
        }
    }
}
