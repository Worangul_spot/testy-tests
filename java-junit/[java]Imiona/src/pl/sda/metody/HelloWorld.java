package pl.sda.metody;

import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * Created by Acer on 2019-01-13.
 */
public class HelloWorld {
    public static void main(String[] args) {
        hello();
        hello("Cokolwiek");
    }

    private static void hello(String cokolwiek) {
        System.out.println("Hello "+ cokolwiek);
    }

    static void hello(){
        System.out.println("Hello Gdynia");
    }
}
